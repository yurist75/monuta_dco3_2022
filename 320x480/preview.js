var previewJSON = {
	"shots": [{
		"shotType": "mastershot",
		"copy": "die paar|euro|per maand|settings|textPosition_topleft|white",
		"cta": "meer informatie",
		"priceValue": "",
		"fontScale": "1",
		"images": [{
				"url": "image1.jpg",
				"transforms": "10x50x1x0"
			},
			{
				"url": "",
				"transforms": "0x0x1x0"
			},
			{
				"url": "",
				"transforms": "0x0x1x0"
			}
		],
		"video": "",
		"clickUrl": ""
	}, {
		"shotType": "mastershot",
		"copy": "geef ik|in de kroeg|ook zo uit|settings|textPosition_bottomright|white",
		"cta": "meer informatie",
		"priceValue": "",
		"fontScale": "",
		"images": [{
				"url": "image2.jpg",
				"transforms": "10x0x1x0"
			},
			{
				"url": "",
				"transforms": "0x0x1x0"
			},
			{
				"url": "",
				"transforms": "0x0x1x0"
			}
		],
		"video": "",
		"clickUrl": ""
	}, {
		"shotType": "mastershot",
		"copy": "verzeker je|persoonlijke|uitvaart|settings|textPosition_slogan|jum",
		"cta": "meer informatie",
		"priceValue": "",
		"fontScale": "",
		"images": [{
				"url": "",
				"transforms": ""
			},
			{
				"url": "",
				"transforms": ""
			},
		],
		"video": "",
		"clickUrl": ""
	}, {
		"shotType": "mastershot",
		"copy": "open|over|afscheid|settings|textPosition_biglogo|jum",
		"cta": "meer informatie",
		"priceValue": "",
		"fontScale": "",
		"images": [

		],
		"video": "",
		"clickUrl": ""
	}, {
		"shotType": "",
		"copy": "",
		"cta": "",
		"priceValue": "",
		"fontScale": "",
		"images": [

		],
		"video": "",
		"clickUrl": ""
	}],
	"geo_copy": "",
	"till_date": "12/31/2020"
};
var previewData = "";