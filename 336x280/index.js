(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#AAB20D").s().p("AhoG9IAAltQAAgIAEgJQAEgKAGgFIB9htQAGgFAFgKQAEgJAAgIIAAkqIA3g1IAAHsQAAAHgEAKQgEAJgGAFIh9BuQgGAGgEAJQgFAJAAAIIAADgg");
	this.shape.setTransform(35.375,44.475);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#AAB20D").s().p("AAyG9IAAjgQAAgIgEgJQgFgJgGgGIh9huQgGgFgEgJQgEgKAAgHIAAnsIA3A1IAAEqQAAAIAFAJQAEAKAGAFIB9BtQAGAFAEAKQAEAJAAAIIAAFtg");
	this.shape_1.setTransform(10.525,44.475);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#AAB20D").s().p("AhYBZQglglAAg0QAAgzAlglQAlgkAzAAQA0AAAlAkQAkAlAAAzQAAA0gkAlQglAlg0AAQgzAAglglg");
	this.shape_2.setTransform(22.95,25.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,45.9,89), null);


(lib.Symbol6 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6E0038").s().p("AiZDgQgngsAAhEQAAhhBOgzQBBgrBegBIACAAIAAgOQAAgjgUgTQgUgUgjAAQg4AAgxAdIgYhaQAdgQAmgLQAvgMAtAAQBuAAArBGQAfAzAABgIAADBQAABMAHAqIh5AAIgIgzIgGAAIAAABQgqA7hIAAQg7AAgmgtgAgTAVQgsAXAAA1QAAAjARAUQAPATAVgBQASABAPgLQASgMAIgZQADgJAAgPIAAhfIgDAAQgpACgbAPg");
	this.shape.setTransform(19.275,26.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,38.6,53.8), null);


(lib.Symbol5 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6E0038").s().p("AgsErQgXgXgLgiQgKgjAAg5IAAj5Ig+AAIAAhgIA+AAIAAhdICHgrIAACIIBoAAIAABgIhoAAIAADxQABBOA3AAQAPAAATgDIABBqQggAJgzAAQhAgBgjggg");
	this.shape.setTransform(15.05,33.15);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,30.1,66.3), null);


(lib.Symbol4 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6E0038").s().p("AigDVQghgzAAhvIAAk7ICJAAIAAEsQAAB0A2gBQARAAANgLQALgLAGgPQAIgSAAgcIAAlMICIAAIAAFqQAAA6AFBcIAAAIIh1AAIgIg3IgFAAIgBABQgUAdggARQgfARgjAAQhIAAghg0g");
	this.shape.setTransform(19.4,26.45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,38.8,52.9), null);


(lib.Symbol3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6E0038").s().p("AA8EJIAAlGQAAhZg2AAQgRAAgPANQgOANgIAXQgHAUAAAdIAAE9IiIAAIAAlsQAAgkgEhkIgBgUIB2AAIAHA9IAFAAIABgBQAVghAhgSQAfgSAnAAQBNAAAjBAQAaAxAABSIAAFOg");
	this.shape.setTransform(19.675,26.475);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,39.4,53), null);


(lib.Symbol2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6E0038").s().p("AAAENQhcAAg1hHQg0hGAAh+QAAiJBBhJQA3g8BQAAQBaAAA1BIQA0BIAAB7QAACjhWBEQgxAng+AAgAg/AAQAACnBBAAIABAAQAiAAAPg9QAKgqAAhAQAAg8gKgpQgPhBglAAQg/AAAACmg");
	this.shape.setTransform(19.825,26.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,39.7,53.8), null);


(lib.Symbol1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6E0038").s().p("ACyFqIgNj9QgJiXAChYIgDAAQgRBogdB1Ig+EDIhjAAIg8j+Qgdh6gPhoIgDAAQgBA4gKC4IgMD8Ih5AAIA0rTICVAAIA6EGIAcCBQAPBGAHA1IACAAQALhOAjiuIA5kGICVAAIAsLTg");
	this.shape.setTransform(30.35,36.175);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,60.7,72.4), null);


(lib.sh1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F6F4F5").s().p("EgbBA+gMAAAh8/MA2DAAAMAAAB8/g");
	this.shape.setTransform(168,400);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.sh1, new cjs.Rectangle(-5,0,346,800), null);


(lib.emptyMovieClip = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.logo = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.Symbol7();
	this.instance.setTransform(334.35,44.5,1,1,0,0,0,22.9,44.5);

	this.instance_1 = new lib.Symbol6();
	this.instance_1.setTransform(267.35,63.5,1,1,0,0,0,19.2,26.9);

	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(228.3,57,1,1,0,0,0,15.1,33.1);

	this.instance_3 = new lib.Symbol4();
	this.instance_3.setTransform(186.6,63.9,1,1,0,0,0,19.4,26.4);

	this.instance_4 = new lib.Symbol3();
	this.instance_4.setTransform(136.9,63,1,1,0,0,0,19.7,26.4);

	this.instance_5 = new lib.Symbol2();
	this.instance_5.setTransform(88.6,63.5,1,1,0,0,0,19.8,26.9);

	this.instance_6 = new lib.Symbol1();
	this.instance_6.setTransform(30.4,53.55,1,1,0,0,0,30.4,36.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.logo, new cjs.Rectangle(0,0,357.4,90.4), null);


// stage content:
(lib.index = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = false; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [0];
	this.isSingleFrame = false;
	// timeline functions:
	this.frame_0 = function() {
		if(this.isSingleFrame) {
			return;
		}
		if(this.totalFrames == 1) {
			this.isSingleFrame = true;
		}
		var self = this,
			WIDTH = lib.properties.width,
			HEIGHT = lib.properties.height,
			cacheScale = Math.max(window.devicePixelRatio, 1),
			animContDiv = document.getElementById("animation_container"),
			font1_Regular = "NeueHaasGroteskText Pro",
			font2_biglogo = "Myriad Pro",
			font3_cta = "NeueHaasGroteskText Pro Bd",
			MAX_LINES = 4,
			defaultFontSize = 26,
			defaultTextMargin = 5,
			textWidth = 330,
			textHeight = 0,
			lineSpacing = 2,
			wordSpacing = 5,
			textX = 16,
			textY = 106,
			prevTextX, prevTextY,
			ctaTextColor = "#ffffff",
			COLOR_WHITE = "#ffffff",
			COLOR_JUM = "#6E0038",
			ctaFontSize = 11.5,
			ctaBgrColor = "#AAB20D",
			ctaOverColor = "#828C05",
			ctaWidth,
			ctaHeight = 40,
			ctaPadding = 13,
			ctaToX = 206,
			ctaToY = 226,
			ctaToY = ctaToY + ctaHeight / 2,
			logoToX = 16,
			logoToY = 241,
			ctaTL, 
			TEXT_SPACE = ctaToY - ctaHeight / 2,
			defaultShotDuration = 3.5,
			loopID = 0,
		    loopMax = 2,
			logoPlay = false;
			
		// DEFAULTS BEGIN
		animContDiv.style.display = "block";
		self.logo.alpha = 1.0;
		self.logo.x = logoToX;
		self.logo.y = logoToY;
		self.logo.cache(-5, -5, 380, 120, cacheScale);
		self.logo.scale = 0.27;
		self.cta.x = 0;
		self.cta.y = ctaToY;
		self.cta.alpha = 1.0;
		self.images.x = WIDTH*0.5;
		self.images.y = 105;
		// DEFAULTS END
		
		self.getShotData = function(i) {
			if (dynamic.getDynamic("shot_type", i) == "" || dynamic.getDynamic("shot_type", i) == "undefined") {
				return undefined;
			}
			var copy = dynamic.getDynamic("copy", i);
			var settings = copy.split("|settings|")[1].toLowerCase();
		
			var ctaText = dynamic.getDynamic("cta_copy", i).toLowerCase();
			copy = copy.split("|settings|")[0].toLowerCase();
			if (settings != undefined) {
				settings = settings.split("|");
			} else {
				settings = "";
			}
			if (copy == undefined || copy == "undefined" || copy.length == 0) {
				copy = false;
			} else {
				copy = copy.split("|");
			}
			var fontScale = dynamic.getDynamic("font_scale", i);
			if (isNaN(fontScale) || fontScale == "") {
				fontScale = 1.0;
			} else {
				fontScale = Number(fontScale);
			}
		
			var shotLength = defaultShotDuration;
			var data = dynamic.getDynamic("shot_type", i).split("|");
			var textPosition = "bottomleft";
			var imagePosition = "topright";
		
			for (var k = 0; k<data.length; k++) {
				var whichData = data[k].split("_");
				if (whichData[0].toLowerCase() == "shotlength" && whichData[1] != "" && !isNaN(whichData[1])) {
					shotLength = whichData[1];
				}
			}
		
			for (j = 0; j < settings.length; j++) {
				var a = settings[j].split("_");
				var b = a[0].toLowerCase();
				var c = "";
				if (a[1] != undefined && a[1].length > 1) {
					c = a[1].toLowerCase();
				}
				if (b == "textposition" && c.length > 0) {
					textPosition = c;
				}
				if (b == "imageposition" && c.length > 0) {
					imagePosition = c;
				}
			}
		
			var image = false;
			if (!dynamic.isImageEmpty(0, i)) {
				image = "Image" + (i * 3 + 1);
			}
			return {
				"settings": settings,
				"textPos": textPosition,
				"imgPos": imagePosition,
				"copies": copy,
				"font_scale": fontScale,
				"cta_text": ctaText,
				"image": image,
				"shot_length": shotLength
			}
		}
		/*
		self.getShotDuration = function(shot) {
			var duration = defaultShotDuration;
			return duration;
		}
		*/
		self.getShotDuration = function (shot) {
			var duration = shot.shot_length;
			defaultShotDuration = duration;
			if (!shot.shot_length) duration = defaultShotDuration;
			//console.log("duration --> " + duration);
			return duration;
		}
		
		self.playShot = function(shot) {
				
			if (shot.index == 0) {
				//addBorder();
				if (loopID < 1) {animateInCta(shot);}
				
				var d = 0.05;
				if (loopID > 0) {d = 0.9}
			
			
				if (shot.image == false) {
					loopMax = 0;
					gsap.delayedCall(d, function () {
					gsap.set(document.getElementById("video_holder"), {opacity: 0});
					dynamic.playVideo(shot.index);
					gsap.to(document.getElementById("video_holder"), {opacity: 1, duration: 0.25});
						});	
				}	
			
				gsap.to(self.sh1, {duration: 0.9, y: 211, ease: "expo", delay: d});
			
				if (loopID > 0) {d += 0.8} else {d += 1.7;}
				gsap.to(self.cta, {duration: 0.5, scale:1.06, ease: "expo.in", delay: d});
				gsap.to(self.cta, {duration: 0.5, scale:1.00, ease: "expo", delay: d+0.5});	
			
				gsap.to(self.cta, {duration: 0.5, scale:1.12, ease: "expo.in", delay: d+1.0});
				gsap.to(self.cta, {duration: 1.2, scale:1.0, ease: "expo", delay: d+1.5});
			}
		
			if (shot.index == 1) {
				var d = defaultShotDuration;
				gsap.to(self.sh1, {duration: 1.1, y: -100, ease: "expo.in", delay: d});
			}
		
			if (shot.index == 2) {
				var d = 0.0;
			}
			
			if (shot.isLastShot == true) {
				var d = 0.0;
				if (!logoPlay) {
				logoPlay = true;
				gsap.set(self.logo2.children, {alpha: 0, delay: d});
				gsap.set(self.logo2.children, {x: "+=50", delay: d});
				gsap.set(self.logo2, {x:60, delay: d});
				gsap.to(self.logo, {duration: 1.0, alpha: 0, ease: "expo", delay: d+0.4});
				gsap.to(self.logo2.children, {duration: 0.5, alpha: 1, x:"-=50", stagger:0.05, ease: "expo", delay: d+0.6});
				}
			
				d += 1.7;
				gsap.to(self.cta, {duration: 0.5, scale:1.06, ease: "expo.in", delay: d});
				gsap.to(self.cta, {duration: 0.5, scale:1.00, ease: "expo", delay: d+0.5});	
			
				gsap.to(self.cta, {duration: 0.5, scale:1.12, ease: "expo.in", delay: d+1.0, onComplete:function() { logoPlay = false}});
				gsap.to(self.cta, {duration: 1.2, scale:1.0, ease: "expo", delay: d+1.5});
			}
		
			if (shot.image != false) {
				animateInImage(shot);
			} else {
				//animateOutPreviousImages();
			}
			if (shot.copies) {
				animateInCopy(shot);
			} else {
				animateOutPreviousCopies("all");
			}
		
			if (shot.isLastShot && loopID < loopMax && dynamic.shots.length > 1) {	
				var timeout = self.getShotDuration(shot) + 1;
				gsap.delayedCall(timeout, function() {
					loopID++;
					dynamic.currentShot = -1;
					dynamic.playNextShot();
					gsap.to(self.logo2.children, {duration: 0.5, alpha:0, stagger:0.05, ease: "expo.in", delay: 0});
					gsap.to(self.logo, {duration: 1.0, alpha: 1, ease: "expo.in", delay: 0.1});
				});
			}
		
			self.onMouseOver = function() {
				gsap.to(self.cta.children[1], {duration: 0.4, alpha: 0.0, ease: "expo", delay: 0});
				ctaTL.restart();
				if (shot.isLastShot && loopID >= loopMax && dynamic.shots.length > 1 && !logoPlay) {	
					var timeout = 0;
					loopID = 99;
					gsap.to(self.logo2.children, {duration: 0.5, alpha:0, stagger:0.05, ease: "expo.in", delay: 0});
					gsap.to(self.logo, {duration: 1.0, alpha: 1, ease: "expo.in", delay: 0.1});
					gsap.delayedCall(timeout, function() {
						dynamic.currentShot = 1;
						dynamic.playNextShot();
					});
					}
			}
		
			self.onMouseOut = function() {
				gsap.to(self.cta.children[1], {duration: 0.4, alpha: 1.0, ease: "expo", delay: 0});
			}
		
		}
		
		function animateInCopy(shot) {
			var copy = shot.copies;
			var fontScale = shot.font_scale;
			var textMargin = defaultTextMargin;
			var fontSize = defaultFontSize;
			if (fontScale != 1) {
				fontSize *= fontScale;
				fontSize = Math.round(fontSize);
			}
			var copies = [];
			var copyTemp = [];
			var maxLines = MAX_LINES;
			var text_color = COLOR_WHITE;
			var shiftto = -1;
			textX = 16;
			lineSpacing = 2;
			textY = Math.round(HEIGHT - fontSize*(copy.length) - lineSpacing*(copy.length-1)) - 80;
			var lineToShift = -1;
				
			if (shot.settings.indexOf("jum") > -1) {
					text_color = COLOR_JUM;
				} 
		
			for (var i = 0; i < copy.length; i++) {
				var c = copy[i];
				var cont = c.split("_")[1];
				copyTemp.push(c);
			}
		
			copy = copyTemp;
		
			for (i = 0; i < copy.length; i++) {
				c = copy[i];
		
				var fontType = font1_Regular;
				var light = false;
				
				if (shot.settings.indexOf("textposition_topleft") > -1 || shot.settings.indexOf("textposition_bottomleft") > -1) {
					shiftto = lineToShift = 0;
				}
			
				if (shot.settings.indexOf("textposition_topright") > -1 || shot.settings.indexOf("textposition_bottomright") > -1) {
					shiftto = lineToShift = 1;
				}	
			
				if (shot.settings.indexOf("textposition_slogan") > -1) {
					shiftto = lineToShift = 0;
				}
				
		
				var k = c.indexOf("_") + 1;
				c = c.substring(k, c.length);
				
				if (c.length > 0) {
					var t = alignCopy(c, fontSize, self.test, fontType);
											
					var line = { 
						isLight: light,
						lines: t
					};
					copies.push(line);
				}
			}
		
		
			var LINES = [];
			for (i = 0; i < copies.length; i++) {
				c = copies[i];
				var lns = c.lines;
				for (var j = 0; j < lns.length; j++) {
					if (LINES.length < maxLines) {
						var ln = lns[j];
						var obj = {
							line: ln,
							isLight: c.isLight
						}
						LINES.push(obj);
					}
				}
			}
		
			textHeight = LINES.length * (fontSize + lineSpacing) - lineSpacing;
			var m = new cjs.MovieClip();
			self.texts.addChild(m);
			var d = 0.5;
			var s = 0;
		    var tw = 0;
			var maxWidth = 0;
			var currentWidth = 0;
					
					for (i = 0; i < LINES.length; i++) {
					t = new cjs.MovieClip();
					var ln_obj = LINES[i];
					var txt = ln_obj.line;
					fontType = font1_Regular;
					lineSpacing = 2;
						
					if (shot.settings.indexOf("textposition_slogan") > -1) {
						fontSize = 26.5;
						light = true;
						lineSpacing = 0;
						textX = 0;
						textY = 80;
						}
						
					if (shot.settings.indexOf("textposition_biglogo") > -1) {
						fontType = font2_biglogo;
						fontSize = 33;
						light = true;
						lineSpacing = -2;
						textX = 59;
						textY = 113;
					} 
				
					
					if (i == shiftto) { t.x = 30 } else {t.x = 0};
		
					txt = txt.split(" ");
				    var prevX = 0;
		
		            for (j = 0; j < txt.length; j++) {
							var tx = txt[j];
		
							var tf = new cjs.Text(tx, fontSize + "px '" + fontType + "'", text_color);
							tf.textBaseline = "alphabetic";
							tf.y += fontSize - 6;
		
							if (j > 0) {
								tf.x = prevX;
							}
		
							var widt = tf.getMeasuredWidth();
							prevX += Math.floor(widt) + wordSpacing;
							tf.cache(-10, -tf.y - 5, widt + 20, fontSize*2, cacheScale);
							tf.toX = tf.x;
						
							if (i == 0 || i == 2) { tf.x = tf.toX + 30; }
							if (i == 1) { tf.x = tf.toX - 30; }
		
							tf.alpha = 0.0;
							t.addChild(tf);
							if (j == txt.length - 1) {
								t.textWidth = prevX - wordSpacing;
							}
					}
				
					t.y = Math.round(i * (fontSize + lineSpacing));
				/*
				if (shot.settings.indexOf("textposition_bottomleft") > -1 || shot.settings.indexOf("textposition_bottomright") > -1) {
					if (LINES.length == 2) { t.y += (fontSize + lineSpacing) };
				}
				*/
					m.addChild(t);
				
				if (shot.settings.indexOf("textposition_topleft") > -1 || shot.settings.indexOf("textposition_topright") > -1) {
					textY = 12;
					}
				
					m.x = textX;
					m.y = textY;	
				}
		
		
		
			
		if (shot.settings.indexOf("textposition_bottomright") > -1 || shot.settings.indexOf("textposition_topright") > -1 || shot.settings.indexOf("textposition_slogan") > -1) {
			
			for (i = 0; i < m.numChildren; i++) {
			
				var ch = m.children[i];
				
				for (j = 0; j < ch.numChildren; j++) {
				t = ch.children[j];
				currentWidth += t.getMeasuredWidth();
				}
			
				currentWidth += (ch.numChildren - 1)*wordSpacing;
				if (lineToShift == i) { currentWidth += 30;	}
				if (currentWidth > maxWidth ) {maxWidth = currentWidth};
				currentWidth = 0;
			
			}
		
			if (shot.settings.indexOf("textposition_slogan") > -1) { m.x = textX = WIDTH*0.5 - Math.round(maxWidth)*0.5;
				} else { m.x = textX = WIDTH - 16 - Math.round(maxWidth); }
		}
		
		
		for (i = 0; i < m.numChildren; i++) {
			
				var ch = m.children[i];
			
					for (j = 0; j < ch.numChildren; j++) {
						s++;
						t = ch.children[j];
						var del = d + s * 0.2;
						if (t.toX != undefined) {
							gsap.to(t, {duration: 1.3, x: t.toX, ease: "expo", delay: del+0.1});
							gsap.to(t, {duration: 1.0, alpha: 1.0, ease: "expo", delay: del+0.1});
						}
					}
				
			}
			
			
			d += 1.0;
			
			if (self.texts.numChildren > 1) {
				animateOutPreviousCopies(false, prevTextX, prevTextY, d - 1.0, shot);
			}
		}
		
		function animateOutPreviousCopies(removeAllCopies, toX, toY, d, shot) {
			var d = 0;
			if (d == undefined) {
				d = 0;
			}
			var outtime = 0.6;
			var step = 0.1;
			var deltaX;
		
			var removeTill = self.texts.numChildren - 1;
			if (removeAllCopies != undefined && removeAllCopies == "all") {
				removeTill++;
				var del = d + (removeTill + 2) * 0.05;
				outtime = 1.0;
			}
			for (var i = 0; i < removeTill; i++) {
				var m = self.texts.children[i];
				if (toX == undefined || toY == undefined || isBubble == true) {
					m.toX = m.x;
					m.toY = m.y;
				} else {
					m.toX = toX;
					m.toY = toY;
				}
		
				for (var j = 0; j < m.numChildren; j++) {
					var l = m.children[j];
		
					var isLast = false;
					if (j == m.numChildren - 1) {
						isLast = true;
					}
				
					if (shot.index == 2 && loopID < (loopMax+1)) {
					  gsap.to(m, {duration: 2.0, y: "-=700", ease: "expo.inOut", delay: d});
					}
				
					if (j == 0) { deltaX = "+=30" }
					if (j == 1 ) { deltaX = "-=30" }
					if (j == 2) { deltaX = "+=30" }
			
					gsap.to(l, {duration: outtime, alpha: 0.0, delay: d + 0.1 + j * step, onCompleteParams: [isLast, m], onComplete: function(trueFalse, container) {
						if (trueFalse == true) {
							self.texts.removeChild(container);
						}
					} });
				}
			}
		}
		
		function alignCopy(copy, fontSize, testField, fontToUse) {
			testField.font = fontSize + "px '" + fontToUse + "'";
			testField.lineWidth = textWidth;
			testField.lineHeight = fontSize;
			var s = copy.split(" ");
			testField.text = "";
			var totalH = testField.getMeasuredHeight();
			var lines = [""];
			var words = [];
			var lineID = 0;
			for (var i = 0; i < s.length; i++) {
				var t = s[i];
				if (t != "") {
					if (t == "\n") {
						words.push("\n");
					} else if (t.indexOf("\n") > -1) {
						var k = t.split("\n");
						words.push(k[0]);
						words.push("\n");
						if (k[1] != undefined && k[1].length > 1) {
							words.push(k[1]);
						}
					} else {
						words.push(t);
					}
				}
			}
			for (i = 0; i < words.length; i++) {
				var toStr = words[i];
				if (i > 0) {
					toStr = " " + toStr;
				}
				testField.text += toStr;
				if (testField.getMeasuredHeight() > totalH) {
					lines[lineID] = lines[lineID].trim();
					totalH = testField.getMeasuredHeight();
					lineID++;
					lines[lineID] = toStr.trim();
				} else {
					lines[lineID] += toStr;
					lines[lineID] = lines[lineID].trim();
				}
			}
			var totalWidth = testField.getBounds().width;
			if (totalWidth > textWidth) {
				var sc = 1 / (totalWidth / textWidth);
				fontSize *= sc;
			}
		
			testField.text = "";
		
			return lines;
		}
		
		function animateOutPreviousImages() {
			var d = 0.0;
			gsap.to(self.images, {duration: 1.8, y: "-=30", ease: "expo.in", delay: d, onComplete: function() {
				self.images.removeAllChildren();
			} });
		
		}
		
		function animateInImage(shot) {
			var d = 0.0;
			var img = shot.image;
			var im = new cjs.MovieClip();
			dynamic.setImage(im, img, 0.5, 0.5);
			im.children[0].scale = 0.36;
			self.images.addChild(im);
			
			if (shot.index == 0) {
				gsap.to(im, {duration: 4.5, scale: 1.1, ease:"sine", delay: d});
			} else {
				gsap.to(im, {duration: 4.5, scale: 0.9, ease:"sine", delay: d});
			}
		
			if (shot.index == 1) {
				gsap.to(im, {duration: 2.5, y: "-=300", ease:"expo.in", delay: defaultShotDuration-0.1});
				}
		
			if (shot.index > 0 && self.images.numChildren > 1) {
				im.alpha = 0.0;
				gsap.to(im, {duration: 1.0, alpha: 1.0, delay: d + 0.3, onComplete: function() {
					removeChildren(self.images, false);
				} });
			}
		}
		
		function animateInCta(shot) {
			var ctaCopy = shot.cta_text;
			if (ctaCopy == undefined || ctaCopy == "undefined" || ctaCopy.length == 0) {
				ctaCopy = "meer informatie";
			}
		
		
			var cont = new cjs.MovieClip();
			var cont2 = new cjs.MovieClip();
			var fontType = font3_cta;
			var t = new cjs.MovieClip();
			var fontSize = ctaFontSize;
			var tf = new cjs.Text(ctaCopy, fontSize + "px '" + fontType + "'", ctaTextColor);
			tf.textBaseline = "alphabetic";
			tf.textAlign = "center";
			tf.y = fontSize - 4;
			var widt = tf.getMeasuredWidth();
			//widt = Math.ceil(widt);
			t.addChild(tf);
		
			t.y = Math.round(ctaHeight*0.25 - fontSize - 3);
			t.cache(-widt*0.5, -5, widt*2, ctaHeight + 10, cacheScale * 1.5);
			ctaWidth = Math.round(widt + ctaPadding * 2);
			if (ctaWidth % 2 != 0) {
				ctaWidth++;
			}
		
			var b = new cjs.Shape();
			b.graphics.beginFill(ctaBgrColor).drawRoundRect(0, 0, ctaWidth, ctaHeight, 5);	
		
			var b2 = new cjs.Shape();
			b2.graphics.beginFill(ctaOverColor).drawRoundRect(0, 0, ctaWidth, ctaHeight, 5);	
			b2.shadow = new createjs.Shadow("#777777", 0, 2, 5);
		
			cont.addChild(b);
			cont2.addChild(b2);
			self.cta.addChild(cont2, cont, t);
		
			cont.x = cont2.x = -ctaWidth / 2;
			cont.y = cont2.y = -ctaHeight / 2;
		
			t.x = cont.x + ctaWidth*0.5;
		
			self.cta.x = WIDTH + cont.x - 15;
		
			var d = 0.3;
			ctaTL = new gsap.timeline({ paused: true, repeat: 1 });
		}
		
		function addBorder() {
			var brd_t = new cjs.Shape();
			brd_t.graphics.beginFill("#E1DBDF").drawRect(-10, 0, WIDTH + 20, 1);
			var brd_tr = new cjs.Shape();
			brd_tr.graphics.beginFill("#E1DBDF").drawRect(WIDTH - 1, -10, 1, HEIGHT + 20);
			var brd_b = new cjs.Shape();
			brd_b.graphics.beginFill("#E1DBDF").drawRect(-10, HEIGHT - 1, WIDTH + 20, 1);
			var brd_tl = new cjs.Shape();
			brd_tl.graphics.beginFill("#E1DBDF").drawRect(0, -10, 1, HEIGHT + 20);
			self.addChild(brd_t, brd_tr, brd_b, brd_tl);
		}
		function removeChildren(whichMC, deleteAll) {
			var ID = 0;
			if (deleteAll != undefined && deleteAll == false) {
				ID = 1;
			}
			while (whichMC.numChildren > ID) {
				whichMC.removeChild(whichMC.children[0]);
			}
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// content
	this.cta = new lib.emptyMovieClip();
	this.cta.name = "cta";
	this.cta.setTransform(-250,500);

	this.logo = new lib.logo();
	this.logo.name = "logo";
	this.logo.setTransform(16.2,241.15,0.27,0.2697,0,0,0,0.8,0.6);

	this.logo2 = new lib.logo();
	this.logo2.name = "logo2";
	this.logo2.setTransform(360.25,86.2,0.3,0.3,0,0,0,0.8,0.7);

	this.texts = new lib.emptyMovieClip();
	this.texts.name = "texts";

	this.test = new cjs.Text("", "32px 'Plastic Sans'", "#FFFFFF");
	this.test.name = "test";
	this.test.lineHeight = 32;
	this.test.lineWidth = 256;
	this.test.parent = this;
	this.test.setTransform(-298,152);

	this.sh1 = new lib.sh1();
	this.sh1.name = "sh1";

	this.images = new lib.emptyMovieClip();
	this.images.name = "images";
	this.images.setTransform(-100,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.images},{t:this.sh1},{t:this.test},{t:this.texts},{t:this.logo2},{t:this.logo},{t:this.cta}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(-132,140,599.2,660);
// library properties:
lib.properties = {
	id: 'D35D1BC0751E4B588C6A92882073D7BE',
	width: 336,
	height: 280,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D35D1BC0751E4B588C6A92882073D7BE'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}
an.handleFilterCache = function(event) {
	if(!event.paused){
		var target = event.target;
		if(target){
			if(target.filterCacheList){
				for(var index = 0; index < target.filterCacheList.length ; index++){
					var cacheInst = target.filterCacheList[index];
					if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
						cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
					}
				}
			}
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;