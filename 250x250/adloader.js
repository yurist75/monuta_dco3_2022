/* Font Face Observer v2.1.0 - © Bram Stein. License: BSD-3-Clause */(function(){'use strict';var f,g=[];function l(a){g.push(a);1==g.length&&f()}function m(){for(;g.length;)g[0](),g.shift()}f=function(){setTimeout(m)};function n(a){this.a=p;this.b=void 0;this.f=[];var b=this;try{a(function(a){q(b,a)},function(a){r(b,a)})}catch(c){r(b,c)}}var p=2;function t(a){return new n(function(b,c){c(a)})}function u(a){return new n(function(b){b(a)})}function q(a,b){if(a.a==p){if(b==a)throw new TypeError;var c=!1;try{var d=b&&b.then;if(null!=b&&"object"==typeof b&&"function"==typeof d){d.call(b,function(b){c||q(a,b);c=!0},function(b){c||r(a,b);c=!0});return}}catch(e){c||r(a,e);return}a.a=0;a.b=b;v(a)}}
function r(a,b){if(a.a==p){if(b==a)throw new TypeError;a.a=1;a.b=b;v(a)}}function v(a){l(function(){if(a.a!=p)for(;a.f.length;){var b=a.f.shift(),c=b[0],d=b[1],e=b[2],b=b[3];try{0==a.a?"function"==typeof c?e(c.call(void 0,a.b)):e(a.b):1==a.a&&("function"==typeof d?e(d.call(void 0,a.b)):b(a.b))}catch(h){b(h)}}})}n.prototype.g=function(a){return this.c(void 0,a)};n.prototype.c=function(a,b){var c=this;return new n(function(d,e){c.f.push([a,b,d,e]);v(c)})};
function w(a){return new n(function(b,c){function d(c){return function(d){h[c]=d;e+=1;e==a.length&&b(h)}}var e=0,h=[];0==a.length&&b(h);for(var k=0;k<a.length;k+=1)u(a[k]).c(d(k),c)})}function x(a){return new n(function(b,c){for(var d=0;d<a.length;d+=1)u(a[d]).c(b,c)})};window.Promise||(window.Promise=n,window.Promise.resolve=u,window.Promise.reject=t,window.Promise.race=x,window.Promise.all=w,window.Promise.prototype.then=n.prototype.c,window.Promise.prototype["catch"]=n.prototype.g);}());

(function(){function l(a,b){document.addEventListener?a.addEventListener("scroll",b,!1):a.attachEvent("scroll",b)}function m(a){document.body?a():document.addEventListener?document.addEventListener("DOMContentLoaded",function c(){document.removeEventListener("DOMContentLoaded",c);a()}):document.attachEvent("onreadystatechange",function k(){if("interactive"==document.readyState||"complete"==document.readyState)document.detachEvent("onreadystatechange",k),a()})};function t(a){this.a=document.createElement("div");this.a.setAttribute("aria-hidden","true");this.a.appendChild(document.createTextNode(a));this.b=document.createElement("span");this.c=document.createElement("span");this.h=document.createElement("span");this.f=document.createElement("span");this.g=-1;this.b.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.c.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
this.f.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.h.style.cssText="display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;";this.b.appendChild(this.h);this.c.appendChild(this.f);this.a.appendChild(this.b);this.a.appendChild(this.c)}
function u(a,b){a.a.style.cssText="max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;white-space:nowrap;font-synthesis:none;font:"+b+";"}function z(a){var b=a.a.offsetWidth,c=b+100;a.f.style.width=c+"px";a.c.scrollLeft=c;a.b.scrollLeft=a.b.scrollWidth+100;return a.g!==b?(a.g=b,!0):!1}function A(a,b){function c(){var a=k;z(a)&&a.a.parentNode&&b(a.g)}var k=a;l(a.b,c);l(a.c,c);z(a)};function B(a,b){var c=b||{};this.family=a;this.style=c.style||"normal";this.weight=c.weight||"normal";this.stretch=c.stretch||"normal"}var C=null,D=null,E=null,F=null;function G(){if(null===D)if(J()&&/Apple/.test(window.navigator.vendor)){var a=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))(?:\.([0-9]+))/.exec(window.navigator.userAgent);D=!!a&&603>parseInt(a[1],10)}else D=!1;return D}function J(){null===F&&(F=!!document.fonts);return F}
function K(){if(null===E){var a=document.createElement("div");try{a.style.font="condensed 100px sans-serif"}catch(b){}E=""!==a.style.font}return E}function L(a,b){return[a.style,a.weight,K()?a.stretch:"","100px",b].join(" ")}
B.prototype.load=function(a,b){var c=this,k=a||"BESbswy",r=0,n=b||3E3,H=(new Date).getTime();return new Promise(function(a,b){if(J()&&!G()){var M=new Promise(function(a,b){function e(){(new Date).getTime()-H>=n?b(Error(""+n+"ms timeout exceeded")):document.fonts.load(L(c,'"'+c.family+'"'),k).then(function(c){1<=c.length?a():setTimeout(e,25)},b)}e()}),N=new Promise(function(a,c){r=setTimeout(function(){c(Error(""+n+"ms timeout exceeded"))},n)});Promise.race([N,M]).then(function(){clearTimeout(r);a(c)},
b)}else m(function(){function v(){var b;if(b=-1!=f&&-1!=g||-1!=f&&-1!=h||-1!=g&&-1!=h)(b=f!=g&&f!=h&&g!=h)||(null===C&&(b=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent),C=!!b&&(536>parseInt(b[1],10)||536===parseInt(b[1],10)&&11>=parseInt(b[2],10))),b=C&&(f==w&&g==w&&h==w||f==x&&g==x&&h==x||f==y&&g==y&&h==y)),b=!b;b&&(d.parentNode&&d.parentNode.removeChild(d),clearTimeout(r),a(c))}function I(){if((new Date).getTime()-H>=n)d.parentNode&&d.parentNode.removeChild(d),b(Error(""+
n+"ms timeout exceeded"));else{var a=document.hidden;if(!0===a||void 0===a)f=e.a.offsetWidth,g=p.a.offsetWidth,h=q.a.offsetWidth,v();r=setTimeout(I,50)}}var e=new t(k),p=new t(k),q=new t(k),f=-1,g=-1,h=-1,w=-1,x=-1,y=-1,d=document.createElement("div");d.dir="ltr";u(e,L(c,"sans-serif"));u(p,L(c,"serif"));u(q,L(c,"monospace"));d.appendChild(e.a);d.appendChild(p.a);d.appendChild(q.a);document.body.appendChild(d);w=e.a.offsetWidth;x=p.a.offsetWidth;y=q.a.offsetWidth;I();A(e,function(a){f=a;v()});u(e,
L(c,'"'+c.family+'",sans-serif'));A(p,function(a){g=a;v()});u(p,L(c,'"'+c.family+'",serif'));A(q,function(a){h=a;v()});u(q,L(c,'"'+c.family+'",monospace'))})})};"object"===typeof module?module.exports=B:(window.FontFaceObserver=B,window.FontFaceObserver.prototype.load=B.prototype.load);}());

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
var devDynamicContent = {};
var emptyImg = "no.png";

if (Enabler.isInitialized()) {
	waitForInit();
} else {
	Enabler.addEventListener(studio.events.StudioEvent.INIT, waitForInit);
}
function waitForInit() {
	if (Enabler.isPageLoaded()) {
		PreLoadFonts();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, PreLoadFonts);
	}
}

setDevDynamicContent();
//window.addEventListener("load", PreLoadFonts, false);
function PreLoadFonts() {
	var fontsLoaded = 0;
	if (FONTS.length == 0) {
		init();
	} else {
		for (var f = 0; f < FONTS.length; f++) {
			var font1 = new FontFaceObserver(FONTS[f]);
			font1.load().then(function() {
				fontsLoaded++;
				if (fontsLoaded == FONTS.length) {
					init();
				}
			}, function() {});
		}
	}
}
function init() {
	if (typeof dynamicContent === "undefined") {
		dynamicContent = devDynamicContent;
	}
	dynamic.setDynamicContent();
	canvas = document.getElementById("canvas");
	anim_container = document.getElementById("animation_container");
	dom_overlay_container = document.getElementById("dom_overlay_container");
	window.comp = AdobeAn.getComposition(compositionID);
	var lib = comp.getLibrary();
	var dynamicLib = [];
	var imagesArray = [
		dynamicContent.dcodata[0].Image1URL,
		dynamicContent.dcodata[0].Image2URL,
		dynamicContent.dcodata[0].Image3URL,
		dynamicContent.dcodata[0].Image4URL,
		dynamicContent.dcodata[0].Image5URL,
		dynamicContent.dcodata[0].Image6URL,
		dynamicContent.dcodata[0].Image7URL,
		dynamicContent.dcodata[0].Image8URL,
		dynamicContent.dcodata[0].Image9URL,
		dynamicContent.dcodata[0].Image10URL,
		dynamicContent.dcodata[0].Image11URL,
		dynamicContent.dcodata[0].Image12URL,
		dynamicContent.dcodata[0].Image13URL,
		dynamicContent.dcodata[0].Image14URL,
		dynamicContent.dcodata[0].Image15URL
	]
	for (var i = 1; i <= imagesArray.length; i++) {
		if (imagesArray[i - 1] != undefined) {
			dynamicLib.push({
				src: imagesArray[i - 1].Url,
				id: "Image" + i,
				type: createjs.Types.IMAGE
			});
		}
	}
	console.log(dynamicLib);
	for (var i = 0; i < IMAGES.length; i++) {
		dynamicLib.push(IMAGES[i]);
	}
	lib.properties.manifest = dynamicLib;
	var loader = new createjs.LoadQueue(false);
	loader.addEventListener("fileload", function(evt) {
		handleFileLoad(evt, comp);
	});
	loader.addEventListener("complete", function(evt) {
		handleComplete(evt, comp);
	});
	preloadVideos(function() {
		loader.loadManifest(lib.properties.manifest);
	});
}
function addSuffix(url, dcmParameter) {
	var urlSuffix = Enabler.getParameter(dcmParameter);
	var _url = "";
	if (url) {
		var symbol = (url.indexOf("?") > -1) ? "&" : "?";
		if (urlSuffix) {
			while (urlSuffix.charAt(0) == "?" || urlSuffix.charAt(0) == "&") {
				urlSuffix = urlSuffix.substring(1);
			}
			if (urlSuffix.indexOf("?") > -1) {
				urlSuffix = urlSuffix.replace(/\?/g, "&");
			}
		}
		_url = url + symbol + urlSuffix;
	}
	return _url;
}
function setDevDynamicContent() {
	devDynamicContent.dcodata = [{}];
	devDynamicContent.dcodata[0]._id = 0;
	devDynamicContent.dcodata[0].Unique_ID = 0;

	InitPreview();

	devDynamicContent.dcodata[0].Shot1 = getParameterByName('shotType1') + ";" + getParameterByName('copy1') + ";" + getParameterByName("cta1") + ";" + getParameterByName('priceType1') + ";" + getParameterByName('priceValue1') + ";" + getParameterByName('fontScale1') + ";" + getParameterByName('imageCount1');
	devDynamicContent.dcodata[0].Shot2 = getParameterByName('shotType2') + ";" + getParameterByName('copy2') + ";" + getParameterByName("cta2") + ";" + getParameterByName('priceType2') + ";" + getParameterByName('priceValue2') + ";" + getParameterByName('fontScale2') + ";" + getParameterByName('imageCount2');
	devDynamicContent.dcodata[0].Shot3 = getParameterByName('shotType3') + ";" + getParameterByName('copy3') + ";" + getParameterByName("cta3") + ";" + getParameterByName('priceType3') + ";" + getParameterByName('priceValue3') + ";" + getParameterByName('fontScale3') + ";" + getParameterByName('imageCount3');
	devDynamicContent.dcodata[0].Shot4 = getParameterByName('shotType4') + ";" + getParameterByName('copy4') + ";" + getParameterByName("cta4") + ";" + getParameterByName('priceType4') + ";" + getParameterByName('priceValue4') + ";" + getParameterByName('fontScale4') + ";" + getParameterByName('imageCount4');
	devDynamicContent.dcodata[0].Shot5 = getParameterByName('shotType5') + ";" + getParameterByName('copy5') + ";" + getParameterByName("cta5") + ";" + getParameterByName('priceType5') + ";" + getParameterByName('priceValue5') + ";" + getParameterByName('fontScale5') + ";" + getParameterByName('imageCount5');
	devDynamicContent.dcodata[0].ExitURL1 = getParameterByName('clickUrl1');
	devDynamicContent.dcodata[0].ExitURL2 = getParameterByName('clickUrl2');
	devDynamicContent.dcodata[0].ExitURL3 = getParameterByName('clickUrl3');
	devDynamicContent.dcodata[0].ExitURL4 = getParameterByName('clickUrl4');
	devDynamicContent.dcodata[0].ExitURL5 = getParameterByName('clickUrl5');

	devDynamicContent.dcodata[0].GeoCopy = getParameterByName("geo_copy");
	devDynamicContent.dcodata[0].ImageData1 = getParameterByName('imgdata1') + ";" + getParameterByName('imgdata2') + ";" + getParameterByName('imgdata3') + ";";
	devDynamicContent.dcodata[0].ImageData2 = getParameterByName('imgdata4') + ";" + getParameterByName('imgdata5') + ";" + getParameterByName('imgdata6') + ";";
	devDynamicContent.dcodata[0].ImageData3 = getParameterByName('imgdata7') + ";" + getParameterByName('imgdata8') + ";" + getParameterByName('imgdata9') + ";";
	devDynamicContent.dcodata[0].ImageData4 = getParameterByName('imgdata10') + ";" + getParameterByName('imgdata11') + ";" + getParameterByName('imgdata12') + ";";
	devDynamicContent.dcodata[0].ImageData5 = getParameterByName('imgdata13') + ";" + getParameterByName('imgdata14') + ";" + getParameterByName('imgdata15') + ";";

	devDynamicContent.dcodata[0].Image1URL = {};
	devDynamicContent.dcodata[0].Image1URL.Url = getParameterByName('img1');
	devDynamicContent.dcodata[0].Image2URL = {};
	devDynamicContent.dcodata[0].Image2URL.Url = getParameterByName('img2');
	devDynamicContent.dcodata[0].Image3URL = {};
	devDynamicContent.dcodata[0].Image3URL.Url = getParameterByName('img3');

	devDynamicContent.dcodata[0].Image4URL = {};
	devDynamicContent.dcodata[0].Image4URL.Url = getParameterByName('img4');
	devDynamicContent.dcodata[0].Image5URL = {};
	devDynamicContent.dcodata[0].Image5URL.Url = getParameterByName('img5');
	devDynamicContent.dcodata[0].Image6URL = {};
	devDynamicContent.dcodata[0].Image6URL.Url = getParameterByName('img6');

	devDynamicContent.dcodata[0].Image7URL = {};
	devDynamicContent.dcodata[0].Image7URL.Url = getParameterByName('img7');
	devDynamicContent.dcodata[0].Image8URL = {};
	devDynamicContent.dcodata[0].Image8URL.Url = getParameterByName('img8');
	devDynamicContent.dcodata[0].Image9URL = {};
	devDynamicContent.dcodata[0].Image9URL.Url = getParameterByName('img9');

	devDynamicContent.dcodata[0].Image10URL = {};
	devDynamicContent.dcodata[0].Image10URL.Url = getParameterByName('img10');
	devDynamicContent.dcodata[0].Image11URL = {};
	devDynamicContent.dcodata[0].Image11URL.Url = getParameterByName('img11');
	devDynamicContent.dcodata[0].Image12URL = {};
	devDynamicContent.dcodata[0].Image12URL.Url = getParameterByName('img12');

	devDynamicContent.dcodata[0].Image13URL = {};
	devDynamicContent.dcodata[0].Image13URL.Url = getParameterByName('img13');
	devDynamicContent.dcodata[0].Image14URL = {};
	devDynamicContent.dcodata[0].Image14URL.Url = getParameterByName('img14');
	devDynamicContent.dcodata[0].Image15URL = {};
	devDynamicContent.dcodata[0].Image15URL.Url = getParameterByName('img15');

	devDynamicContent.dcodata[0].Video1 = {};
	devDynamicContent.dcodata[0].Video1.Url = getParameterByName('video1');
	devDynamicContent.dcodata[0].Video2 = {};
	devDynamicContent.dcodata[0].Video2.Url = getParameterByName('video2');
	devDynamicContent.dcodata[0].Video3 = {};
	devDynamicContent.dcodata[0].Video3.Url = getParameterByName('video3');
	devDynamicContent.dcodata[0].Video4 = {};
	devDynamicContent.dcodata[0].Video4.Url = getParameterByName('video4');
	devDynamicContent.dcodata[0].Video5 = {};
	devDynamicContent.dcodata[0].Video5.Url = getParameterByName('video5');

	devDynamicContent.dcodata[0].Loop = 0;
	devDynamicContent.dcodata[0].Weight = "";
	devDynamicContent.dcodata[0].Time_From = {};
	devDynamicContent.dcodata[0].Time_From.RawValue = "";
	devDynamicContent.dcodata[0].Time_From.UtcValue = 0;
	devDynamicContent.dcodata[0].Time_Till = {};
	devDynamicContent.dcodata[0].Time_Till.RawValue = "";
	devDynamicContent.dcodata[0].Time_Till.UtcValue = 0;
	devDynamicContent.dcodata[0].Date_From = {};
	devDynamicContent.dcodata[0].Date_From.RawValue = "";
	devDynamicContent.dcodata[0].Date_From.UtcValue = 0;
	devDynamicContent.dcodata[0].Date_Till = {};
	devDynamicContent.dcodata[0].Date_Till.RawValue = getParameterByName('till_date');
	devDynamicContent.dcodata[0].Date_Till.UtcValue = 0;
	devDynamicContent.dcodata[0].ExitURL = {};
	devDynamicContent.dcodata[0].ExitURL.Url = "";
	devDynamicContent.dcodata[0].Brand = {};
	devDynamicContent.dcodata[0].Brand.Url = emptyImg;

	Enabler.setDevDynamicContent(devDynamicContent);
}
function InitPreview() {
	previewData = {};
	if (getParameterByName('shotType1') != "") {
		return;
	}
	var totalIndex = 1;
	for (var s = 0; s < previewJSON.shots.length; s++) {
		var shot = previewJSON.shots[s];
		var index = s + 1;
		insertParam("shotType" + index, shot.shotType);
		insertParam("copy" + index, shot.copy);
		insertParam("cta" + index, shot.cta);
		insertParam("priceValue" + index, shot.priceValue);
		insertParam("fontScale" + index, shot.fontScale);
		insertParam("clickUrl" + index, shot.clickUrl);
		insertParam("video" + index, shot.video);
		for (var i = 0; i < shot.images.length; i++) {
			var url = emptyImg;
			insertParam("imgdata" + totalIndex, shot.images[i].transforms);
			if (shot.images[i].url != "") { 
				url = shot.images[i].url;
			}
			insertParam("img" + totalIndex, url);
			totalIndex++;
		}
		insertParam("imageCount" + index, shot.images.length);
	}
	insertParam("geo_copy", previewJSON.geo_copy);
	insertParam("till_date", previewJSON.till_date);
	for (var i = (totalIndex - 1); i < 16; i++) {
		var url = emptyImg;
		insertParam("imgdata" + i, "0x0x1x0");
		insertParam("img" + i, url);
	}
	function insertParam(key, value) {
		var j = "&";
		if (previewData == "") { 
			j = "?";
		}
		previewData += j + key + "=" + value;
	}
}
function preloadVideos(onComplete) {
	var videosToPreload = [];
	var videosLoaded = 0;
	for (var v = 1; v <= 1; v++) {
		var videoURL = dynamicContent.dcodata[0]['Video' + v];
		if (videoURL != undefined) {
			videoURL = videoURL.Url;
			if (videoURL.indexOf("/.mp4") == -1 && videoURL.indexOf("empty.mp4") == -1 && videoURL != undefined && videoURL != null && videoURL != "") {
				var existingVideo = dynamic.findVideoElement(videoURL);
				if (existingVideo == undefined) {
					dynamic.createVideoElement(videoURL);
					videosToPreload.push(dynamic.videoElements[v - 1]);
				} else {
					dynamic.videoElements.push(existingVideo);
				}
			}
		}
	}
	onComplete();
}
function handleFileLoad(evt, comp) {
	var images = comp.getImages();
	if (evt && (evt.item.type == "image")) {
		images[evt.item.id] = evt.result;
	}
}
function handleComplete(evt, comp) {
	var lib = comp.getLibrary();
	var ss = comp.getSpriteSheet();
	var queue = evt.target;
	var ssMetadata = lib.ssMetadata;
	for (i = 0; i < ssMetadata.length; i++) {
		ss[ssMetadata[i].name] = new createjs.SpriteSheet({
			"images": [queue.getResult(ssMetadata[i].name)],
			"frames": ssMetadata[i].frames
		});
	}
	exportRoot = new lib.index();
	stage = new lib.Stage(canvas);
	fnStartAnimation = function() {
		stage.addChild(exportRoot);
		createjs.Ticker.framerate = lib.properties.fps;
		createjs.Ticker.addEventListener("tick", stage);
		dynamic.onInit();
	}
	AdobeAn.makeResponsive(false, "both", false, 1, [canvas, anim_container, dom_overlay_container]);
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimation();
}
function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	if (previewData.length > 0) url = window.location.href + previewData;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
	if (!results) return '';
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}