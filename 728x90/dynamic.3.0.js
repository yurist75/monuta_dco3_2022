var dynamic = {
	currentMovie: null,
	currentShot: -1,
	container: null,
	width: 0,
	height: 0,
	dynamicData: [],
	shots: [],
	bVideoWasPlayed: false,
	currentVideo: undefined,
	videoElements: [],
	max_shots: 5,
	onInit: function() {
		this.width = comp.getLibrary().properties.width;
		this.height = comp.getLibrary().properties.height;
		stage.update();
		this.currentMovie = exportRoot;
		this.container = document.getElementById("animation_container");
		this.container.style.cursor = "pointer";
		this.container.addEventListener("click", this.onMouseClick);
		this.container.addEventListener("mouseover", this.onMouseOver.bind(this));
		this.container.addEventListener("mouseout", this.onMouseOut.bind(this));
		this.container.addEventListener("mousemove", this.onMouseMove.bind(this));
		for (var i = 0; i < this.max_shots; i++) {
			var shotData = exportRoot.getShotData(i);
			if (shotData != undefined) {
				shotData.index = i;
				this.shots.push(shotData);
			}
		}
		if (this.shots.length >= 1) {
			this.shots[this.shots.length - 1].isLastShot = true;
			this.playNextShot();
		}
	},
	getCurrentShot: function() {
		return this.shots[this.currentShot];
	},
	playNextShot: function() {
		if (this.currentShot < this.shots.length - 1) {
			this.currentShot++;
			var shot = this.shots[this.currentShot];
			var timeout = exportRoot.getShotDuration(shot);
			gsap.killTweensOf(this.callNextShot);
			if (this.shots[this.currentShot].isLastShot != true) {
				gsap.delayedCall(timeout, this.callNextShot);
			}
			console.log("Playing Shot #" + (this.currentShot + 1));
			exportRoot.playShot(shot);
		}
	},
	callNextShot: function() {
		dynamic.playNextShot();
	},
	onMouseMove: function(e) {
		if (this.currentMovie.onMouseMove != undefined) {
			this.currentMovie.onMouseMove(e);
		}
	},
	onMouseOver: function(e) {
		if (this.currentMovie.onMouseOver != undefined) {
			this.currentMovie.onMouseOver(e);
		}
	},
	onMouseOut: function() {
		if (this.currentMovie.onMouseOut != undefined) {
			this.currentMovie.onMouseOut();
		}
	},
	onMouseClick: function() {
		var exitURL = addSuffix(dynamic.getDynamic("url"));
		var res = exitURL.replace("[size]", dynamic.width + "x" + dynamic.height);
		Enabler.exitOverride("ExitEvent", res);
	},
	createVideoElement: function(videoURL) {
		var videoTagAttributes = " muted playsinline";
		if (dynamic.loopVideo == true) {
			videoTagAttributes += " loop";
		}
		document.getElementById("video_holder").innerHTML = "<video src='" + videoURL + "'" + videoTagAttributes + " preload='none'>";
		var newVideo = document.getElementsByTagName("video")[0];
		newVideo.load();
		newVideo.style.display = "none";
		this.videoElements.push(newVideo);
	},
	findVideoElement: function(theUrl) {
		for (var v = 0; v < dynamic.videoElements.length; v++) {
			if (dynamic.videoElements[v] == undefined) {
				continue;
			}
			var fileName1 = dynamic.videoElements[v].src.replace(/^.*[\\\/]/, '');
			var fileName2 = theUrl.replace(/^.*[\\\/]/, '');
			if (fileName1 == fileName2) {
				return dynamic.videoElements[v];
			}
		}
		return undefined;
	},
	playVideo: function(videoIndex) {
		var self = dynamic;
		var newVideo = self.videoElements[videoIndex];
		newVideo.style.display = "block";
		newVideo.play();
		self.currentVideo = newVideo;
		if (self.bVideoWasPlayed == false) {
			gsap.delayedCall(30.0, function() {
				for (var v = 0; v < self.videoElements.length; v++) {
					if (self.videoElements[v] == undefined) {
						continue;
					}
					self.videoElements[v].pause();
				}
			});
		}
		self.bVideoWasPlayed = true;
	},
	getImageIndex: function(indx, shotIndex) {
		indx = parseInt(indx);
		if (shotIndex == undefined) {
			shotIndex = this.currentShot;
		}
		var startFromIndex = 0;
		for (var i = 0; i < this.dynamicData.length; i++) {
			if (i < shotIndex) {
				startFromIndex += parseInt(this.dynamicData[i].image_count);
			}
		}
		return indx + startFromIndex;
	},
	setImage: function(copyTo, image, regX, regY) {
		if (regX == undefined) {
			regX = 0.5;
		}
		if (regY == undefined) {
			regY = 0.5;
		}
		copyTo.removeAllChildren();
		var theIndex = image;
		if (!isNaN(theIndex)) {
			if (theIndex < this.max_shots * 3) {
				theIndex = this.getImageIndex(image);
			}
		}
		var img = this.getImage(theIndex);
		var bitmap = new createjs.Bitmap(img);
		if (bitmap == undefined || bitmap.image == undefined) {
			return false;
		}
		bitmap.regX = bitmap.image.width * regX;
		bitmap.regY = bitmap.image.height * regY;
		var n = theIndex;
		var img_str = "image";
		if (typeof n == "string" && n.toLowerCase().indexOf(img_str) > -1) {
			n = n.toLowerCase();
			n = n.substring(img_str.length, n.length);
			theIndex = Number(n) - 1;
		}
		if (!isNaN(theIndex) && theIndex < this.max_shots * 3) {
			var imageData = this.getImageData(theIndex);
			bitmap.scale = Number(imageData.scale);
			bitmap.rotation = Number(imageData.rotation);
			bitmap.x = Number(Math.round(imageData.x));
			bitmap.y = Number(Math.round(imageData.y));
		}
		copyTo.addChild(bitmap);
	},
	getImage: function(img) {
		var images = comp.getImages();
		if (!isNaN(img)) {
			img = "Image" + (img + 1);
		}
		return images[img];
	},
	getImageURL: function(index) {
		return comp.getLibrary().properties.manifest[index].src;
	},
	isImageEmpty: function(imageIndex, shotID) {
		if (shotID == undefined) {
			shotID = this.currentShot;
		}
		var imgIndex = this.getImageIndex(imageIndex, shotID);
		var theImage = this.getImage(imgIndex);
		if (theImage == undefined || theImage.width == 1 && theImage.height == 1) {
			return true;
		}
		return false;
	},
	getImageData: function(index) {
		for (var i = 1; i <= this.max_shots; i++) {
			if (dynamicContent.dcodata[0]['ImageData' + i] == undefined) {
				dynamicContent.dcodata[0]['ImageData' + i] = "0x0x1x0;0x0x1x0;0x0x1x0";
			}
		}
		var feedContent = dynamicContent.dcodata[0].ImageData1 + dynamicContent.dcodata[0].ImageData2 + dynamicContent.dcodata[0].ImageData3 + dynamicContent.dcodata[0].ImageData4 + dynamicContent.dcodata[0].ImageData5;
		var imagesArray = feedContent.split(';');
		if (imagesArray[index] == undefined) {
			return { x: 0, y: 0, scale: 1, rotation: 0 }
		}
		var valuesArray = imagesArray[index].split('x');
		var newObject = {
			x: valuesArray[0],
			y: valuesArray[1],
			scale: valuesArray[2],
			rotation: valuesArray[3]
		};
		return newObject;
	},
	getTillDate: function() {
		return dynamicContent.dcodata[0].Date_Till.RawValue;
	},
	getCounterDays: function() {
		var maxDays = 7;
		var oneDay = 24 * 60 * 60 * 1000;
		var firstDate = new Date();
		var secondDate = new Date(this.getTillDate());
		var diffDays = Math.ceil((secondDate.getTime() - firstDate.getTime()) / (oneDay));
		return diffDays;
	},
	getDynamic: function(valueID, shotID) {
		if (this.dynamicData.length <= shotID) {
			return "";
		}
		if (shotID == undefined) {
			if (valueID.indexOf("image") != -1) {
				return {
					x: 0,
					y: 0,
					scale: 1,
					rotation: 0
				};
			}
			shotID = this.getCurrentShot().index;
		}
		if (valueID.indexOf("image_data_") != -1) {
			return this.getImageData(0 + 3 * (shotID));
		} else if (valueID == "shot_type") {
			var line = this.dynamicData[shotID].shot_type;
			if (line != undefined) {
				return line.replace(/<br\s*[\/]?>/gi, "\n");
			} 
			return "";
		} else if (valueID == "font_scale") {
			return this.dynamicData[shotID].font_scale;
		} else if (valueID == "copy") {
			var line = this.dynamicData[shotID].copy;
			if (line != undefined) {
				return line.replace(/<br\s*[\/]?>/gi, "\n");
			}
			return "";
		} else if (valueID == "geo_copy") {
			var line = dynamicContent.dcodata[0].GeoCopy;
			if (line != undefined) {
				return line.replace(/<br\s*[\/]?>/gi, "\n");
			}
			return "";
		} else if (valueID == "cta_copy") {
			var line = this.dynamicData[shotID].cta_copy;
			if (line != undefined) {
				return line.replace(/<br\s*[\/]?>/gi, "\n");
			}
			return "";
		} else if (valueID == "url" || valueID == "exit_url") {
			return this.dynamicData[shotID].url.Url;
		} else if (valueID == "till_date") {
			return this.getTillDate();
		} else if (valueID == "price" || valueID == "price_type") {
			return this.dynamicData[shotID].price_copy;
		} else {
			console.log("Sorry, but there is no dynamic value of this name: " + valueID);
		}
	},
	setDynamicContent: function() {
		if (typeof(dynamicContent) == "undefined" || dynamicContent.dcodata == undefined) {
			return;
		}
		this.loadMovies(dynamicContent.dcodata[0]);
	},
	loadMovies: function(data) {
		for (var i = 1; i <= this.max_shots; i++) {
			if (data['Shot' + i] != undefined && data['Shot' + i] != "") {
			this.dynamicData.push(this.parseDynamicData(data['Shot' + i], data['ExitURL' + i]));
			}
		}
	},
	parseDynamicData: function(d, url) {
		var valuesArray = d.split(';');
		var newObject = {
			shot_type: valuesArray[0],
			copy: valuesArray[1],
			cta_copy: valuesArray[2],
			price_type: valuesArray[3],
			price_copy: valuesArray[4],
			font_scale: valuesArray[5],
			image_count: valuesArray[6],
			url: url
		}
		return newObject;
	},
}